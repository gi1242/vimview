# vimview: Lightweight imlib2 + Gtk based single image viewer

## DEPENDENCIES

    imlib2
    Gtk+-2
    libexif

## INSTALL

    cd src; env NO_DEBUG=1 CFLAGS=-O2 make && mv vimview /usr/bin/

## USAGE

Sorry. No (complete) documentation yet. Use `--help` for a list of options.
When viewing images (some) key bindings are:

    h/l	Pan left / right (on zoomed image). Ctrl/Shift affect speed
    j/k	Pan up / down (on zoomed image). Ctrl/Shift affect speed
    1	Zoom to original size
    x	Fit image to window (preserving aspect)
    [/]	Rotate left/right
    +/-	Zoom in / out. (Can also use '=' for zoom in).
    q	Quit
    w	Fit window to image (buggy on Fvwm2).

## LICENCE

    GPL-v3 or later.

## AUTHOR

    Gautam Iyer <gi+remove+1242@gm+this+ail.com>

    Created  : Tue 09 Oct 2007 06:19:47 PM PDT
    Modified : Mon 22 Aug 2016 09:52:26 AM EDT

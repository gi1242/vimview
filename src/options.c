#include "vimview.h"

/*
 * Command line option definitions.
 */
#define OPTION( lname, sname, flags, arg, adata, desc, adesc ) \
    {lname, sname, flags, arg, adata, desc, adesc}


GOptionEntry options[] = {
    OPTION( "background", 'b', 0, G_OPTION_ARG_INT, &v.defaultBG,
	    "Background color", "0xNNNNNN" ),
    OPTION( "geometry", 'g', 0, G_OPTION_ARG_STRING, &v.geomstring,
	    "Initial geometry of window", "geometry" ),
    OPTION( "fit-image", 'i', 0, G_OPTION_ARG_NONE, &v.fitImage,
	    "Fit image to window", NULL ),
    OPTION( "max-win-size", 'm', 0, G_OPTION_ARG_INT, &v.maxWinSize,
	    "Max % of screen image should occupy", "percent" ),
    OPTION( "zoom", 'z', 0, G_OPTION_ARG_DOUBLE, &v.defaultZoom,
	    "Initial zoom", "factor" ),

    OPTION( "debug-mask", 'd', 0, G_OPTION_ARG_INT, &v.msgMask,
	    "Mask of debug messages to show", "mask" ),

    /* Last option */
    OPTION( NULL, '\0', 0, 0, NULL, NULL, NULL )
};


void
initOptionValues()
{
    v.msgMask	    = DEBUG_ALL & ~DBG_FUNCTRACE;
    v.defaultZoom   = 0.;
    v.fitImage	    = FALSE;
    v.maxWinSize    = 80;
    v.defaultBG	    = 0x000000ul;
    v.geomstring    = NULL;
}

void
correctOptionValues()
{
    if( v.fitImage && v.defaultZoom != 0 )
    {
	v.defaultZoom = 0.;
	infoMessage( "Ignoring user zoom value in place of --fit-image" );
    }

    if( v.geomstring )
	v.fitImage = TRUE;
}

#include "vimview.h"
#include <string.h>
#include <stdlib.h>
#include <gdk/gdkx.h>

/*
 * Global variables
 */
vimview_t   v;
img_t	    img;


/*
 * Functions
 */
void
die(const char *fmt, ...)
{
    va_list	    ap;

    va_start( ap, fmt);

    fprintf( stderr, APL_NAME " fatal error: " );
    vfprintf( stderr, fmt, ap );
    fprintf( stderr, "\n" );

    va_end( ap );

    gtk_main_quit();
    exit(1); /* NOT REACHED */
}


void
message( unsigned int mask, const char *fmt, ... )
{
    va_list	    ap;

    if( v.msgMask & mask )
    {
	va_start( ap, fmt);

	//fprintf( stderr, APL_NAME ": " );
	vfprintf( stderr, fmt, ap );
	fprintf( stderr, "\n" );

	va_end( ap );
    }
}


void
init()
{
    v.dpy	= GDK_DISPLAY();
    v.vis	= GDK_VISUAL_XVISUAL( gdk_visual_get_system() );
    v.cmap	= GDK_COLORMAP_XCOLORMAP(  gdk_colormap_get_system() );
    v.depth	= DefaultDepth( v.dpy, DefaultScreen( v.dpy ) );
}

int
main( int argc, char *argv[] )
{
    GError *gerror=NULL;

    /*
     * Set our global structures to 0 before calling init functions.
     */
    memset( &v, 0, sizeof(v) );
    memset( &img, 0, sizeof(img) );

    /*
     * Initialize.
     */
    initOptionValues();
    gtk_init_with_args( &argc, &argv, NULL, options, NULL, &gerror );
    if( gerror )
    {
	puts( gerror->message );
	exit(1);
    }
    init();
    correctOptionValues();

    if( argv[1] == NULL )
    {
	errorMessage( "Bad usage. See --help for more options" );
	exit(1);
    }

    if( !imgShow( argv[1] ) )
    {
	errorMessage( "Could not load %s", argv[1] );
	exit(1);
    }

    gtk_main();

    return 0;
}
